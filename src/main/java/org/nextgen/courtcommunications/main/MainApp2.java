package org.nextgen.courtcommunications.main;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.nextgen.courtcommunications.camel.TestBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp2 {
	
	public static void main(String[] args) throws Exception {
		
		 ConfigurableApplicationContext appContext = new ClassPathXmlApplicationContext(
	                "classpath:META-INF/spring/context-camel.xml");
		 CamelContext camelContext = SpringCamelContext.springCamelContext(
	                appContext, false);
	        try {
	        	 TestBean testBean = (TestBean) appContext.getBean("testBean");
	             System.out.println(testBean.hello("Camel and Spring"));
	            camelContext.start();
	            Thread.sleep(1000);
	        } finally {
	        	camelContext.stop();
	            appContext.close();
	        }
	    }
		
}
