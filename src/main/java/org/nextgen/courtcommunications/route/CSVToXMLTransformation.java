package org.nextgen.courtcommunications.route;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.nextgen.courtcommunications.process.XMLTransformation;


public class CSVToXMLTransformation {
	
	public static void main(String[] args) throws Exception {
		CamelContext _ctx=new DefaultCamelContext();
		_ctx.addRoutes(new RouteBuilder(){
		public void configure() throws Exception
		{
		from("file:src/main/resources/input/data")
		.process(new XMLTransformation())
		.to("file:src/main/resources/output?fileName=record.xml");
		}
		});
		_ctx.start();
		Thread.sleep(4000);
		_ctx.stop();
		}

}
//return new ClassPathXmlApplicationContext(); 