package org.nextgen.courtcommunications.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Record_Type_03 {
	
	 private String id;

	 private String record_Action;

	 private String cTN;

	 private String sID;
	
	 private String cS_apa;

	 private String dfn_name;
	
	 private String dfn_address;

	 private String dfn_city;

	 private String dfn_state;

	 private String dfn_zip;
	
	 private String habitualNotice;
	
	 private String advisoryNotice;

	 private String sex;
	
	 private String dOB;
	
	 private String race;	 
	
	 private String ev_Date;
	
	 private String ev_action;
	
	 private String ev_date_arraign;
	
	 private String cS_dccourt;
	
	 private String cS_dfct;
	
	 private String cS_pacaseno;

	 private String df_driverlic;

	 private String df_state;

	 private String df_lpd;
	
	 private String df_code1;
	
	 private String df_ctn;
	
	 private String df_ssn;
	
	 private String df_fname;
	
	 private String df_lname;
	
	 private String wr_custody;
	 
	 	
	 public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}



	public String getRecord_Action() {
		return record_Action;
	}



	public void setRecord_Action(String record_Action) {
		this.record_Action = record_Action;
	}



	public String getcTN() {
		return cTN;
	}



	public void setcTN(String cTN) {
		this.cTN = cTN;
	}



	public String getsID() {
		return sID;
	}



	public void setsID(String sID) {
		this.sID = sID;
	}



	public String getcS_apa() {
		return cS_apa;
	}



	public void setcS_apa(String cS_apa) {
		this.cS_apa = cS_apa;
	}



	public String getDfn_name() {
		return dfn_name;
	}



	public void setDfn_name(String dfn_name) {
		this.dfn_name = dfn_name;
	}



	public String getDfn_address() {
		return dfn_address;
	}



	public void setDfn_address(String dfn_address) {
		this.dfn_address = dfn_address;
	}



	public String getDfn_city() {
		return dfn_city;
	}



	public void setDfn_city(String dfn_city) {
		this.dfn_city = dfn_city;
	}



	public String getDfn_state() {
		return dfn_state;
	}



	public void setDfn_state(String dfn_state) {
		this.dfn_state = dfn_state;
	}



	public String getDfn_zip() {
		return dfn_zip;
	}



	public void setDfn_zip(String dfn_zip) {
		this.dfn_zip = dfn_zip;
	}



	public String getHabitualNotice() {
		return habitualNotice;
	}



	public void setHabitualNotice(String habitualNotice) {
		this.habitualNotice = habitualNotice;
	}



	public String getAdvisoryNotice() {
		return advisoryNotice;
	}



	public void setAdvisoryNotice(String advisoryNotice) {
		this.advisoryNotice = advisoryNotice;
	}



	public String getSex() {
		return sex;
	}



	public void setSex(String sex) {
		this.sex = sex;
	}



	public String getdOB() {
		return dOB;
	}



	public void setdOB(String dOB) {
		this.dOB = dOB;
	}



	public String getRace() {
		return race;
	}



	public void setRace(String race) {
		this.race = race;
	}



	public String getEv_Date() {
		return ev_Date;
	}



	public void setEv_Date(String ev_Date) {
		this.ev_Date = ev_Date;
	}



	public String getEv_action() {
		return ev_action;
	}



	public void setEv_action(String ev_action) {
		this.ev_action = ev_action;
	}



	public String getEv_date_arraign() {
		return ev_date_arraign;
	}



	public void setEv_date_arraign(String ev_date_arraign) {
		this.ev_date_arraign = ev_date_arraign;
	}



	public String getcS_dccourt() {
		return cS_dccourt;
	}



	public void setcS_dccourt(String cS_dccourt) {
		this.cS_dccourt = cS_dccourt;
	}



	public String getcS_dfct() {
		return cS_dfct;
	}



	public void setcS_dfct(String cS_dfct) {
		this.cS_dfct = cS_dfct;
	}



	public String getcS_pacaseno() {
		return cS_pacaseno;
	}



	public void setcS_pacaseno(String cS_pacaseno) {
		this.cS_pacaseno = cS_pacaseno;
	}



	public String getDf_driverlic() {
		return df_driverlic;
	}



	public void setDf_driverlic(String df_driverlic) {
		this.df_driverlic = df_driverlic;
	}



	public String getDf_state() {
		return df_state;
	}



	public void setDf_state(String df_state) {
		this.df_state = df_state;
	}



	public String getDf_lpd() {
		return df_lpd;
	}



	public void setDf_lpd(String df_lpd) {
		this.df_lpd = df_lpd;
	}



	public String getDf_code1() {
		return df_code1;
	}



	public void setDf_code1(String df_code1) {
		this.df_code1 = df_code1;
	}



	public String getDf_ctn() {
		return df_ctn;
	}



	public void setDf_ctn(String df_ctn) {
		this.df_ctn = df_ctn;
	}



	public String getDf_ssn() {
		return df_ssn;
	}



	public void setDf_ssn(String df_ssn) {
		this.df_ssn = df_ssn;
	}



	public String getDf_fname() {
		return df_fname;
	}



	public void setDf_fname(String df_fname) {
		this.df_fname = df_fname;
	}



	public String getDf_lname() {
		return df_lname;
	}



	public void setDf_lname(String df_lname) {
		this.df_lname = df_lname;
	}



	public String getWr_custody() {
		return wr_custody;
	}



	public void setWr_custody(String wr_custody) {
		this.wr_custody = wr_custody;
	}



	@Override
	    public String toString()
	    {
	        return "ClassPojo [CS_dfct = "+cS_dfct+", Df_state = "+df_state+", Df_fname = "+df_fname+", DOB = "+dOB+", Ev_Date = "+ev_Date+", Dfn_name = "+dfn_name+", AdvisoryNotice = "+advisoryNotice+", Df_driverlic = "+df_driverlic+", HabitualNotice = "+habitualNotice+", CS_pacaseno = "+cS_pacaseno+", CS_dccourt = "+cS_dccourt+", Df_ctn = "+df_ctn+", Sex = "+sex+", Df_ssn = "+df_ssn+", Race = "+race+", Df_lpd = "+df_lpd+", Wr_custody = "+wr_custody+", Dfn_address = "+dfn_address+", Record_Action = "+record_Action+", Df_lname = "+df_lname+", Dfn_state = "+dfn_state+", SID = "+sID+", Dfn_zip = "+dfn_zip+", Ev_action = "+ev_action+", Ev_date_arraign = "+ev_date_arraign+", Df_code1 = "+df_code1+", Dfn_city = "+dfn_city+", CTN = "+cTN+", Id = "+id+", CS_apa = "+cS_apa+"]";
	    }
	}
