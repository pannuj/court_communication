package org.nextgen.courtcommunications.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD) 
public class Root {	
	
	@XmlElement(name="record_Type_02")	
	private Record_Type_02 Record_Type_02;	
	
	@XmlElement(name = "record_Type_03")	
    private Record_Type_03 Record_Type_03; 
	
	@XmlElement(name="record_Type_04")	
	private Record_Type_04 Record_Type_04;    
   

	public Record_Type_04 getRecord_Type_04 ()
    {
        return Record_Type_04;
    }

    public void setRecord_Type_04 (Record_Type_04 Record_Type_04)
    {
        this.Record_Type_04 = Record_Type_04;
    }

    public Record_Type_03 getRecord_Type_03 ()
    {
        return Record_Type_03;
    }

    public void setRecord_Type_03 (Record_Type_03 Record_Type_03)
    {
        this.Record_Type_03 = Record_Type_03;
    }

    public Record_Type_02 getRecord_Type_02 ()
    {
        return Record_Type_02;
    }

    public void setRecord_Type_02 (Record_Type_02 Record_Type_02)
    {
        this.Record_Type_02 = Record_Type_02;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Record_Type_04 = "+Record_Type_04+", Record_Type_03 = "+Record_Type_03+", Record_Type_02 = "+Record_Type_02+"]";
    }
}
	
	