package org.nextgen.courtcommunications.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class Record_Type_04 {
	
	 private String id;
	
	 private String record_Action;
	
	 private String cTN;
	 
	 private String sID;
	
	 private String courtCount1;
	
	 private String cG_count;
	
	 private String cG_finalcharge;
	
	 private String cG_finalatmpt;
	
	 private String notice_Pacccode;
	 
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getRecord_Action() {
		return record_Action;
	}


	public void setRecord_Action(String record_Action) {
		this.record_Action = record_Action;
	}




	public String getCourtCount1() {
		return courtCount1;
	}


	public void setCourtCount1(String courtCount1) {
		this.courtCount1 = courtCount1;
	}

	public String getcTN() {
		return cTN;
	}


	public void setcTN(String cTN) {
		this.cTN = cTN;
	}


	public String getsID() {
		return sID;
	}


	public void setsID(String sID) {
		this.sID = sID;
	}


	public String getcG_count() {
		return cG_count;
	}


	public void setcG_count(String cG_count) {
		this.cG_count = cG_count;
	}


	public String getcG_finalcharge() {
		return cG_finalcharge;
	}


	public void setcG_finalcharge(String cG_finalcharge) {
		this.cG_finalcharge = cG_finalcharge;
	}


	public String getcG_finalatmpt() {
		return cG_finalatmpt;
	}


	public void setcG_finalatmpt(String cG_finalatmpt) {
		this.cG_finalatmpt = cG_finalatmpt;
	}


	public String getNotice_Pacccode() {
		return notice_Pacccode;
	}


	public void setNotice_Pacccode(String notice_Pacccode) {
		this.notice_Pacccode = notice_Pacccode;
	}


	@Override
    public String toString()
    {
        return "ClassPojo [SID = "+sID+", CourtCount1 = "+courtCount1+", CG_finalatmpt = "+cG_finalatmpt+", Notice_Pacccode = "+notice_Pacccode+", CTN = "+cTN+", CG_count = "+cG_count+", Id = "+id+", CG_finalcharge = "+cG_finalcharge+", Record_Action = "+record_Action+"]";
    }
	

}
