package org.nextgen.courtcommunications.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.nextgen.courtcommunications.model.Root;


public class RouteProcessor implements Processor {

	public void process(Exchange exchange) throws Exception {
		Root root = exchange.getIn().getBody(Root.class);		
		exchange.getIn().setBody(root);
		
		
		System.out.println("Inside Process----"+root);
		System.out.println("Record Id 2: " + root.getRecord_Type_02().getId());
		
	}

}
