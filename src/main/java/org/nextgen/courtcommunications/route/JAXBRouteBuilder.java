package org.nextgen.courtcommunications.route;

import javax.xml.bind.JAXBContext;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.dataformat.beanio.BeanIODataFormat;
import org.apache.camel.spi.DataFormat;
import org.nextgen.courtcommunications.model.Root;
import org.nextgen.courtcommunications.process.RouteProcessor;
import org.nextgen.courtcommunications.process.XMLTransformation;

public class JAXBRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		// XML Data Format
		JaxbDataFormat xmlDataFormat = new JaxbDataFormat();
		JAXBContext con = JAXBContext.newInstance(Root.class);
		xmlDataFormat.setContext(con);
		
		DataFormat recordDataformat = new BeanIODataFormat("mapping/camel-beanio.xml", "recordStream");
		

		// JSON Data Format
		//JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Root.class);

		from("file:src/main/resources/input").routeId("firstRoute").doTry().unmarshal(xmlDataFormat).process(new RouteProcessor()).marshal(recordDataformat).

				to("file:src/main/resources/output?fileName=Record5.txt").doCatch(Exception.class).process(new Processor() {

					public void process(Exchange exchange) throws Exception {
						Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
						System.out.println(cause);
					}
				});
		
		from("file:src/main/resources/input/data").routeId("secondRoute")
		.process(new XMLTransformation())
		.to("file:src/main/resources/output?fileName=record3.xml");
	}

}
