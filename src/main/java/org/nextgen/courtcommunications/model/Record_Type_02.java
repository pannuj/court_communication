package org.nextgen.courtcommunications.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class Record_Type_02  {	
	 
	 private String id;	 
	
	 private String record_Action;
	 
	 private String cTN;
	 
	 private String sID;
	 
	 private String cS_class_type;
	 
	 private String cS_agency;
	
	 private String arresting_officer;
	
	 private String compliant_nbr;
	
	 private String complainant;
	
	 private String crime_date;	 
	
	 private String cS_location;
	
	 private String cS_warrun_date;
	
	 private String nbr_def;

	 private String cS_spcode;

	 private String cS_unit;
	
	 private String cS_dv;
	
	 private String pA_ORI_State;
	
	 private String pA_ORI;
	
	 private String court_ORI_State;
	
	 private String court_ORI; 	 
	
	
	 public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getRecord_Action() {
		return record_Action;
	}



	public void setRecord_Action(String record_Action) {
		this.record_Action = record_Action;
	}



	public String getcTN() {
		return cTN;
	}



	public void setcTN(String cTN) {
		this.cTN = cTN;
	}



	public String getsID() {
		return sID;
	}



	public void setsID(String sID) {
		this.sID = sID;
	}



	public String getcS_class_type() {
		return cS_class_type;
	}



	public void setcS_class_type(String cS_class_type) {
		this.cS_class_type = cS_class_type;
	}



	public String getcS_agency() {
		return cS_agency;
	}



	public void setcS_agency(String cS_agency) {
		this.cS_agency = cS_agency;
	}



	public String getArresting_officer() {
		return arresting_officer;
	}



	public void setArresting_officer(String arresting_officer) {
		this.arresting_officer = arresting_officer;
	}



	public String getCompliant_nbr() {
		return compliant_nbr;
	}



	public void setCompliant_nbr(String compliant_nbr) {
		this.compliant_nbr = compliant_nbr;
	}



	public String getComplainant() {
		return complainant;
	}



	public void setComplainant(String complainant) {
		this.complainant = complainant;
	}



	public String getCrime_date() {
		return crime_date;
	}



	public void setCrime_date(String crime_date) {
		this.crime_date = crime_date;
	}



	public String getcS_location() {
		return cS_location;
	}



	public void setcS_location(String cS_location) {
		this.cS_location = cS_location;
	}



	public String getcS_warrun_date() {
		return cS_warrun_date;
	}



	public void setcS_warrun_date(String cS_warrun_date) {
		this.cS_warrun_date = cS_warrun_date;
	}



	public String getNbr_def() {
		return nbr_def;
	}



	public void setNbr_def(String nbr_def) {
		this.nbr_def = nbr_def;
	}



	public String getcS_spcode() {
		return cS_spcode;
	}



	public void setcS_spcode(String cS_spcode) {
		this.cS_spcode = cS_spcode;
	}



	public String getcS_unit() {
		return cS_unit;
	}



	public void setcS_unit(String cS_unit) {
		this.cS_unit = cS_unit;
	}



	public String getcS_dv() {
		return cS_dv;
	}



	public void setcS_dv(String cS_dv) {
		this.cS_dv = cS_dv;
	}



	public String getpA_ORI_State() {
		return pA_ORI_State;
	}



	public void setpA_ORI_State(String pA_ORI_State) {
		this.pA_ORI_State = pA_ORI_State;
	}



	public String getpA_ORI() {
		return pA_ORI;
	}



	public void setpA_ORI(String pA_ORI) {
		this.pA_ORI = pA_ORI;
	}



	public String getCourt_ORI_State() {
		return court_ORI_State;
	}



	public void setCourt_ORI_State(String court_ORI_State) {
		this.court_ORI_State = court_ORI_State;
	}



	public String getCourt_ORI() {
		return court_ORI;
	}



	public void setCourt_ORI(String court_ORI) {
		this.court_ORI = court_ORI;
	}



	@Override
	    public String toString()
	    {
	        return "ClassPojo [CS_spcode = "+cS_spcode+", CS_unit = "+cS_unit+", Court_ORI_State = "+court_ORI_State+", PA_ORI = "+pA_ORI+", Crime_date = "+crime_date+", CS_dv = "+cS_dv+", Record_Action = "+record_Action+", SID = "+sID+", Arresting_officer = "+arresting_officer+", CS_location = "+cS_location+", Compliant_nbr = "+compliant_nbr+", CS_warrun_date = "+cS_warrun_date+", CS_agency = "+cS_agency+", CTN = "+cTN+", PA_ORI_State = "+pA_ORI_State+", Id = "+id+", Nbr_def = "+nbr_def+", Complainant = "+complainant+", CS_class_type = "+cS_class_type+"]";
	    }
	
}
