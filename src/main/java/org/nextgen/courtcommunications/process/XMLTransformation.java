package org.nextgen.courtcommunications.process;

import javax.xml.stream.XMLStreamWriter;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class XMLTransformation implements Processor {

	public void process(Exchange exchange) throws Exception {
		 String myString = exchange.getIn().getBody(String.class);
	     String[] lineSeparator = myString.split(System.getProperty("line.separator"));
	     XMLStreamWriter xmlWriter = null;
	 	 String st = new String();
	     
	     StringBuffer sb = new StringBuffer();
	        sb.append("<Root>");
	        for (String lineData : lineSeparator) 
	        {
	        String [] commaSeparator=lineData.split("~");
	    	st = commaSeparator[0];
			
			switch (st) {
			case "06":
				
				sb.append("<record_Type_06>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<eventRecord>" + commaSeparator[4].toString()+"</eventRecord>");
				sb.append("<circuitCourt>" + commaSeparator[5].toString()+"</circuitCourt>");
				sb.append("<eventDate>" + commaSeparator[6].toString()+"</eventDate>");
				sb.append("<eventTime>" + commaSeparator[7].toString()+"</eventTime>");
				sb.append("<eventAction>" + commaSeparator[8].toString()+"</eventAction>");
				sb.append("<judgeBar>" + commaSeparator[9].toString()+"</judgeBar>");
				sb.append("<defAttyBar>" + commaSeparator[10].toString()+"</defAttyBar>");
				sb.append("<location>" + commaSeparator[11].toString()+"</location>");
				sb.append("</record_Type_06>");
			break;
			
			case "07":
				
				sb.append("<record_Type_07>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<status>" + commaSeparator[4].toString()+"</status>");
				sb.append("<bond>" + commaSeparator[5].toString()+"</bond>");
				sb.append("</record_Type_07>");
			break;
			
			case "08":
				
				sb.append("<record_Type_08>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<count>" + commaSeparator[4].toString()+"</count>");
				sb.append("<authCharge>" + commaSeparator[5].toString()+"</authCharge>");
				sb.append("<auth>" + commaSeparator[6].toString()+"</auth>");
				sb.append("<finalCharge>" + commaSeparator[7].toString()+"</finalCharge>");
				sb.append("<finalz>" + commaSeparator[8].toString()+"</finalz>");
				sb.append("<dispo>" + commaSeparator[9].toString()+"</dispo>");
				sb.append("<dispDate>" + commaSeparator[10].toString()+"</dispDate>");
				sb.append("<dispEvent>" + commaSeparator[11].toString()+"</dispEvent>");
				sb.append("<courtCount>" + commaSeparator[12].toString()+"</courtCount>");
				sb.append("<notice>" + commaSeparator[13].toString()+"</notice>");
				sb.append("</record_Type_08>");
			break;
			
			case "09":
	
				sb.append("<record_Type_08>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<count>" + commaSeparator[4].toString()+"</count>");
				sb.append("<sentenceType>" + commaSeparator[5].toString()+"</sentenceType>");
				sb.append("<sentenceDate>" + commaSeparator[6].toString()+"</sentenceDate>");
				sb.append("<sentenceLengthInDays>" + commaSeparator[7].toString()+"</sentenceLengthInDays>");
				sb.append("<sentenceLengthInMonths>" + commaSeparator[8].toString()+"</sentenceLengthInMonths>");
				sb.append("<sentenceLengthInYears>" + commaSeparator[9].toString()+"</sentenceLengthInYears>");
				sb.append("<sentenceCredit>" + commaSeparator[10].toString()+"</sentenceCredit>");
				sb.append("<sentenceAmt>" + commaSeparator[11].toString()+"</sentenceAmt>");
				sb.append("<sentenceTerm>" + commaSeparator[12].toString()+"</sentenceTerm>");
				sb.append("</record_Type_09>");
			break;

			case "10":
	
				sb.append("<record_Type_10>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<count>" + commaSeparator[4].toString()+"</count>");
				sb.append("<sentenceType>" + commaSeparator[5].toString()+"</sentenceType>");
				sb.append("<sentenceDate>" + commaSeparator[6].toString()+"</sentenceDate>");
				sb.append("<sentenceLengthMin>" + commaSeparator[7].toString()+"</sentenceLengthMin>");
				sb.append("<sentenceLengthMax>" + commaSeparator[8].toString()+"</sentenceLengthMax>");
				sb.append("<probation>" + commaSeparator[9].toString()+"</probation>");
				sb.append("<sentenceCredit>" + commaSeparator[10].toString()+"</sentenceFine>");
				sb.append("<sentenceFine>" + commaSeparator[11].toString()+"</sentenceAmt>");
				sb.append("<sentenceTerm>" + commaSeparator[12].toString()+"</sentenceTerm>");
				sb.append("<amtRestitution>" + commaSeparator[13].toString()+"</amtRestitution>");
				sb.append("</record_Type_10>");
			break;

			case "81":
	
				sb.append("<record_Type_81>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<habitualNotice>" + commaSeparator[4].toString()+"</habitualNotice>");
				sb.append("<advisoryNotice>" + commaSeparator[5].toString()+"</advisoryNotice>");
				sb.append("</record_Type_81>");
			break;

			case "91":
				
				sb.append("<record_Type_91>");
				sb.append("<id>" + commaSeparator[0].toString()+"</id>");
				sb.append("<record_Action>" + commaSeparator[1].toString()+"</record_Action>");
				sb.append("<cTN>" + commaSeparator[2].toString()+"</cTN>");
				sb.append("<sID>" + commaSeparator[3].toString()+"</sID>");
				sb.append("<count>" + commaSeparator[4].toString()+"</count>");
				sb.append("<sentenceType>" + commaSeparator[5].toString()+"</sentenceType>");
				sb.append("<noteNumber>" + commaSeparator[6].toString()+"</noteNumber>");
				sb.append("<note>" + commaSeparator[7].toString()+"</note>");
				sb.append("</record_Type_91>");
			break;						
		
	}
	        }
			 sb.append("</Root>");
			 System.out.println("MyProcessor complete");
		     exchange.getIn().setBody(sb.toString());

	        }
	}
